#include "linealregresion.h"
#include <iostream>
#include <Eigen/Dense>
#include <string>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <stdlib.h>
#include <fstream>

/*En esta clase se desarrolla la funcion OLS y  gradiente descendiente
 *Tal y como ha sido demostrado en clase.
 */

/* Se necesita entrenar el modelo, lo que significa
 * que necesitamos minimizar, por lo tanto requerimos una funcion
 * de costo, la idea es medir la precision de la funcion de hipotesis.
 * La funcion de costo es la forma de penalizar al modelo por cometer un error
 * Se implementa una funcion que retorna un flotante, que toma como entradas
 * el DATASET (x,y) junti con los coeficientes (m1,m2....,mn,b)
 */

 float LinealRegresion::FunCostOLS (Eigen::MatrixXd X,Eigen::MatrixXd y, Eigen::MatrixXd Theta)
{
   Eigen::MatrixXd diferencia = pow((X*Theta-y).array(),2);
   return (diferencia.sum())/(2*X.rows());
}

/*Se necesita proveer al programa una funcion para dar al algoritmo
 * un valor inicial para Theta, el cual cambiara iterativamente
 * hasta que converja el valor al minimo de nuestra funcion
 * de costos. Basicamente describe el G&D: La idea es calcular
 * el gradiente para la funcion de costos dado por la derivada parcial
 * de la funcion. La funcion tendra un alfa que representa el salto
 * del gradiente. Las entradas para la funcion sera X,Y,Theta y el numero de veces
 * para actualizar Theta hasta que la funcion converja
 *
 */
std::tuple <Eigen::VectorXd, std::vector<float>> LinealRegresion::GradDesc(Eigen::MatrixXd X,Eigen::MatrixXd y,
                                                                           Eigen::VectorXd Theta, float alpha,  int iteraciones){
  /* Se almacena temporalmente los parametros de Theta*/
    Eigen::MatrixXd temporalTheta = Theta;
  /*Se extrae la cantidad de parametros (m.features) o val independientes*/
    int parametros = Theta.rows();
    /*Se ubica el costo inicial, que se actualiza con cada paso y
     * los nuevos pesos */
    std::vector<float> costo;
    costo.push_back(FunCostOLS(X,y,Theta));
    /* Por cada iteracion se calcula la funcion de error, que se usa para
    * multiplicar cada dimension o feature y asi almacenarlo en la variable temporal.
    * Se actualiza theta y se calcula el nuevo valor de la funcion de costo basada en el nuevo error de theta
    */

    for (int i=0; i<iteraciones;i++){
        Eigen::MatrixXd error = X*Theta-y;
        for(int j=0;j<parametros;j++){
            Eigen::MatrixXd X_i = X.col(j);
            Eigen::MatrixXd valorTemp = error.cwiseProduct(X_i);
            temporalTheta(j,0) =  Theta(j,0)-(alpha/X.rows()*valorTemp.sum());
        }
        Theta=temporalTheta;
        costo.push_back(FunCostOLS(X,y,Theta));
    }
    /*Se empaqueta la tupla para ser entregada*/
   return std::make_tuple(Theta,costo);

}
/* Para determinar que tan bueno es el modelo que se ha desarrollado a continuación
se implementa una función como metrica de evaluación: R2. R2 representa una medida
de que tan bueno es el modelo. y_hat son las predicciones.
La funcion va a retornar un flotante */
float LinealRegresion::RSquared(Eigen::MatrixXd y, Eigen::MatrixXd y_hat){
    auto numerador = pow((y-y_hat).array(), 2).sum();
    auto denominador = pow(y.array()-y.mean(), 2).sum();

    return 1-(numerador/denominador);
}















