/*
    *Fecha:02/03/2022
    *Autor:Daniel Felipe Campuzano R.
    *Materia: HPC2-Metricas de Rendimiento
    *Tema: Introduccion a machine learning
    *Objetivo: Funcion principal para el calculo del modelo de regresion lineal
    *Requerimientos:
    *1.- Aplicacion para la lectura de ficheros (CSV),
    *Presente en una clase, para la extraccion de los datos, la normalizacion
    *de los datos, en general para la manipulacion de los datos.
    *2.- Crear una clase para el calculo de la regresion
    *lineal.

*/


#include <iostream>
#include <Eigen/Dense>
#include <string>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <fstream>
#include "EXTRACT/extraerdata.h"
#include "linealregresion.h"

int main(int argc, char *argv[])
{
     /*Se crea un objeto tipo extraerdata*/
    ExtraerData extraer(argv[1],argv[2],argv[3]);
    /*Se crea un objeto de tipo linear regresion*/
    LinealRegresion LR;

   /*Leer datos del objeto extraer*/
    std::vector<std::vector<std::string>> DataFrame = extraer.ReadCSV();

    /*Para Probar la funcion EigentOfILE Y DE ESA MANERA IMPRIMIR EL FICHERO DE DATOS
    se debe definir el numero de filas y columnas del dataset*/

    int filas = DataFrame.size() + 1;
    int columnas = DataFrame[0].size();

    std::cout<<" Cantidad de filas: "<<filas<<std::endl;
    std::cout<<" Cantidad de columnas: "<<columnas<<std::endl;
    Eigen::MatrixXd Dataset = extraer.CSVtoEigen(DataFrame,filas,columnas);

    /*Se imprime el vector de promedios por columna */

    Eigen::MatrixXd PDataset=extraer.Promedio(Dataset) ;

     /*Se imprime el vector de promedios por columna */
    std::cout <<"-------------------------PROMEDIO-----------------------"<< std::endl;
    Eigen::MatrixXd DvDataset=extraer.DesvStand(Dataset) ;
    std::cout<<PDataset<<columnas<<std::endl;
    std::cout <<"-------------------------DESVIACION-----------------------"<< std::endl;
    std::cout<<DvDataset<<columnas<<std::endl;

    /*Se crea una matriz para almacenar la data normalizada*/
    Eigen::MatrixXd DataNormalizado = extraer.Normalizador(Dataset);
    /*Se imprime los Datos Normalizados*/
    std::cout<<DataNormalizado<<std::endl;
    /* A continuacion se dividen en grupos de entrenamiento y
     * pruebas la matriz DataNormalizado.
     * Se tomara para entrenamiento el 80% de los datos.
     */
    std::tuple<Eigen::MatrixXd,Eigen::MatrixXd,Eigen::MatrixXd,Eigen::MatrixXd> divDatos=extraer.TrainTestSplit(DataNormalizado,0.8);
    Eigen::MatrixXd X_train;
    Eigen::MatrixXd y_train;
    Eigen::MatrixXd X_test;
    Eigen::MatrixXd y_test;
    std::tie(X_train, y_train, X_test,y_test)=divDatos;
    /*Se imprime el numero de filas de cada uno de los elementos*/
    std::cout<<DataNormalizado.rows()<<std::endl;

    std::cout<<"Cantidad de X_train"<<std::endl;
    std::cout<<X_train.rows()<<std::endl;
    std::cout<<"Cantidad de y_train"<<std::endl;
    std::cout<<y_train.rows()<<std::endl;
    std::cout<<"Cantidad de X_test"<<std::endl;
    std::cout<<X_test.rows()<<std::endl;
    std::cout<<"Cantidad de y_test"<<std::endl;
    std::cout<<y_test.rows()<<std::endl;


    /* A continuacion se desarrolla el primer algoritmo de machine learning
     * regresion Lineal, Para el algoritmo se usa como ejemplo
     *  el dataset de vino rojo el cual tiene multiples variables.
     *  Dada la naturaleza de RL, si se tiene variables con dif unidades
     *  ordenes de magnitud, una variable podria beneficiar/ perjudicar a otra variable
     *  Para ello se recomienda estandarizar los datos, dando a todas las variables el mismo orden de magnitud
     *  y centradas en cero. Es importante recalcar que la clase artesanal para la manipulacion/ tratamiento de datos
     *  debe ser observada de cerca, a la hora de utilizar cualquier otro dataset.

     */
     /* Se implemena el modulo de machine learning como calse de RL
     * con su interfaz, se define constructor y todas las funciones o metodos necesarios
     * para la RL.
     * Se tiene en cuenta que la RL es un metdo estadistico que define la relacion entre
     *  var independientes y var dependiente. La idea principal es definir una linea
     *  recta.
     */

    /*A continuacion se define un vector para entrenamiento y prueba
     *  con valor inicial de 1.
    */

    Eigen::VectorXd vectorTrain = Eigen::VectorXd::Ones(X_train.rows());
    Eigen::VectorXd vectorTest = Eigen::VectorXd::Ones(X_test.rows());
    /*Se re dimensionan las matrices para ubicarlas en el vector de unos
     * creado anteriormente. Similar a la funcion Reshape de numpy.
     */
    X_train.conservativeResize(X_train.rows(),X_train.cols()+1);
    X_train.col(X_train.cols()-1) =  vectorTrain;

    X_test.conservativeResize(X_test.rows(),X_test.cols()+1);
    X_test.col(X_test.cols()-1) =  vectorTest;


    /* Se define el vector theta*/
    Eigen::VectorXd theta = Eigen::VectorXd::Zero(X_train.cols());
    Eigen::VectorXd theta2 = Eigen::VectorXd::Zero(X_test.cols());

    /* Se define alpha como ratio	 de aprendizaje (salto)*/
    float alpha = 0.005;
    int iteraciones = 1000;

    /* De igual forma se procederá a desempaquetar la tupla
     Dada por el objeto (modelo)*/
    std::tuple<Eigen::VectorXd, std::vector<float>> gradiente = LR.GradDesc(X_train, y_train,theta,alpha,iteraciones);
    std::tuple<Eigen::VectorXd, std::vector<float>> gradiente2 = LR.GradDesc(X_test, y_test,theta2,alpha,iteraciones);
    Eigen::VectorXd thetaSalida;
    std::vector<float> costo;
    Eigen::VectorXd thetaSalida2;
    std::vector<float> costo2;
    std::tie(thetaSalida, costo) = gradiente;
    std::tie(thetaSalida2, costo2) = gradiente2;

     /*Se imprime el vector de coeficientes o pesos */
    std::cout << thetaSalida << std::endl;

    /* Se imprime el vector de costo, para apreciar como decrementa su valor*/
    for (auto v: costo){
        std::cout << v << std::endl;
    }

    std::cout << thetaSalida2 << std::endl;

    /* Se imprime el vector de costo, para apreciar como decrementa su valor*/
    for (auto v: costo2){
        std::cout << v << std::endl;
    }

    // Se exporta los valores de la funcion de costos y los coeficientes de theta a ficheros.
    extraer.conVectorFichero(costo, "VectorCosto.txt");
    extraer.EigentoFile(thetaSalida, "VectorTheta.txt");
    extraer.conVectorFichero(costo2, "VectorCosto_test.txt");
    extraer.EigentoFile(thetaSalida2, "VectorTheta_test.txt");
    /* Se calcula de nuevo el promedio y la desviacion estandar basada en los datos
    para calcular  y_hat (predicciones). */
    auto promedioData = extraer.Promedio(Dataset);
    auto numFeatures = promedioData(0,6);
    auto escalados = Dataset.rowwise()-Dataset.colwise().mean();
    auto sigmaData = extraer.DesvStand(escalados);
    auto sigmaFeatures = sigmaData(0, 6);

    Eigen::MatrixXd y_train_hat = (X_train*thetaSalida*sigmaFeatures).array() + numFeatures;
    Eigen::MatrixXd y_test_hat = (X_test*thetaSalida2*sigmaFeatures).array() + numFeatures;
    Eigen::MatrixXd y = Dataset.col(6).topRows(43152);
    Eigen::MatrixXd y2 = Dataset.col(6).topRows(10788);
    /* A continuación se determina que tan bueno es nuestro modelo. */
    float R2 = LR.RSquared(y, y_train_hat);
    float R2_t = LR.RSquared(y2, y_test_hat);
    std::cout <<"-------------------------Metrica R2 Train-----------------------"<< std::endl;
    std::cout << R2 << std::endl;
    std::cout <<"-------------------------Metrica R2 Test-----------------------"<< std::endl;
    std::cout << R2_t << std::endl;
    extraer.EigentoFile(y_train_hat, "y_train_hat.txt");
    extraer.EigentoFile(y_test_hat, "y_test_hat.txt");
    return EXIT_SUCCESS;

}
