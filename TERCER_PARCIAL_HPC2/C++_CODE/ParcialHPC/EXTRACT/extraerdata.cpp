/*
    *Fecha:02/03/2022
    *Autor:Daniel Felipe Campuzano R.
    *Materia: HPC2-Metricas de Rendimiento
    *Tema: Introduccion a machine learning
    *Objetivo: Funcion principal para el calculo del modelo de regresion lineal
    *Requerimientos:
    *1.- Aplicacion para la lectura de ficheros (CSV),
    *Presente en una clase, para la extraccion de los datos, la normalizacion
    *de los datos, en general para la manipulacion de los datos.
    *API para la lectura y manipulacion de un fichero CSV


*/
#include "extraerdata.h"
#include <iostream>
#include <Eigen/Dense>
#include <string>
#include <boost/algorithm/string.hpp>
#include <vector>
#include <stdlib.h>
#include <fstream>

/*
    *Primer funcion kiembro: Lectura fichero CSV
    *Vector de Vectores "String" . La idea es leer
    *linea por linea y almacenar en un vector de vectores
    *de tipo "String "
*/


std::vector<std::vector<std::string>> ExtraerData::ReadCSV(){
    /*Se abre fichero para lectura solamente*/
    std::ifstream Fichero(setDatos);
    /*Vector de vetores "String": tendra los datos del dataset*/
    std::vector<std::vector<std::string>> datosString;
    /*Se itera a traves de cada linea del dataset,
    *al tiempo que se divide la linea con el delimitador
    */
    //Se almacena la linea
    std::string linea= "";
    while(getline(Fichero,linea)){
        std::vector<std::string> vectorFila;
        //Dividimos segun el delimitador
        boost::algorithm::split(vectorFila,linea,boost::is_any_of(delimitador));
        datosString.push_back(vectorFila);
    }
    //se cierra el fichero
    Fichero.close();
    //se retorna el vector de vector string
    return datosString;
}

/*Segunda funcion para guardar vector de vectores de tipo string
    *Se almacena en una matriz para presentar como
    *un objeto parecido al DATAFRAME que entrega PANDAS
*/

Eigen::MatrixXd ExtraerData:: CSVtoEigen(std::vector<std::vector<std::string>> setDatos, int filas, int columnas){
    if(header==true){
        filas = filas-1;
    }
    Eigen::MatrixXd dfMatriz(columnas,filas);
    int i,j;
    for(i=0;i< filas;i++){
        for (j=0;j<columnas;j++){
            //con atof se para del tipo float los string
            dfMatriz(j,i) = atof(setDatos[i][j].c_str());
        }
    }
    /*Se transpone la matriz para que sea filas x columnas
     * se devuelve o retorna la matrix
     * */
    return dfMatriz.transpose();
}

/* Se hace la funcion que retorne el promedio por cada dato
 * columna por columna. La idea es comparar con lo hecho en python-pandas
 * sklearn para verificar que la funcion artesanal corresponda (validar)*/
/*Auto y decltype, la cual la deducira de forma automatica con su
 *inicializador (tiempo de compilacion), para las funciones si el tipo de retorno es un auto se evaluara mediante la expresion
 *del tipo de retorno en tiempo de compilacion
*/
auto ExtraerData::Promedio(Eigen::MatrixXd datos) -> decltype(datos.colwise().mean()){
    return datos.colwise().mean();
}
/*Funcion de desviacion Standart
  *datas = xi-x.promedio
  *
*/
auto ExtraerData::DesvStand(Eigen::MatrixXd data)-> decltype((data.array().square().colwise().sum()/(data.rows()-1)).sqrt()){
    return (data.array().square().colwise().sum()/(data.rows()-1)).sqrt();
}
/*Acto seguido se procede a hacer el calculo por la funcion de normalizacion
 *la idea es evitar los cambios en orden de magnitud, lo anterior representa
 *un deterioro para la prediccion, sobre la base de cualquier modelo de
 *machine learning. (Evitar los outliers).
*/

Eigen::MatrixXd ExtraerData::Normalizador (Eigen::MatrixXd datos){
    /*Normalizacion
    *DataEscalado = xi - x.mean()/ desviacionEstandar
    */
    /*Primero se extrae el promedio  */
    Eigen::MatrixXd DataEscalado = datos.rowwise() - Promedio(datos);
    /*Primero se extrae la desviacion  */
    /*Se retorna cada dato escalado */
    Eigen::MatrixXd MatrixNorm = DataEscalado.array().rowwise()/DesvStand(DataEscalado);
    return MatrixNorm;
}

/*Se implemeta funcion de division de datos,
la idea es crear 4 matrices que tengan los datos de las variables
para entrenamiento y pruebas */

/*Seria seimilar a la funcion train_test_split*/

std::tuple<Eigen::MatrixXd,Eigen::MatrixXd,Eigen::MatrixXd,Eigen::MatrixXd>
ExtraerData::TrainTestSplit(Eigen::MatrixXd dataNorm, float sizeTrain){
    /*Numero de Filas*/
    int filas = dataNorm.rows();
     /*Numero de filas para entrenamiento*/
    int filasTrain = round(sizeTrain * filas);
    /*Numero de filas para prueba*/
    int filasTest = filas-filasTrain;
    /*Con eigen se puede especificar un bloque de una matriz superior
    * o inferior a partir de la fila que quieras como final del bloque
    * o como principio del bloque. Para este caso en especial se seleccionara
    * como entrenamiento el bloque superior de la matriz dataNorm.
    * Se deja la matriz inferior para prueba
    *
*/
    Eigen::MatrixXd Train = dataNorm.topRows(filasTrain);
    /* Para este caso en especial (dataset de entrada)
     * se tiene que los datos de las columnas se identifican
     * en la parte izquierda las features o variables independientes
     * quedando la primera columna de la derecha como variable
     * dependiente.
     */
    /* Se crea una matriz correspondiente a las features o
     * variables independientes */
    Eigen::MatrixXd X_train = Train.leftCols(dataNorm.cols()-1);
    /* Se crea una matriz correspondiente a la variable
    * dependiente a la primera columna
    */
    Eigen::MatrixXd y_train = Train.rightCols(1);
    /* Se hace el mismo procedimiento para prueba */
    Eigen::MatrixXd Test = dataNorm.bottomRows(filasTest);
    Eigen::MatrixXd X_test = Test.leftCols(dataNorm.cols()-1);
    Eigen::MatrixXd y_test = Test.rightCols(1);

    /* Se retorna la tupla dada por el conjunto de
     * datos de entrenamiento y de prueba.
     * Se empaqueta la tupla con la funcion make_tuple,
     * la cual debe ser desempaquetada en la funcion principal.
     */
    return std::make_tuple(X_train,y_train,X_test,y_test);

}

/* A continuacion se desarollan dos funciones para la manipulacion de vectores
 * y la conversion fichero a matriz Eigen. La manipulacion de vectores representa
 * iterar por el fichero de entrada y convertirlo a flotantes.
 */

void ExtraerData::conVectorFichero(std::vector<float> vectorDatos,std::string fileName){
    /* Se crea un objeto que tendra la lectura del fichero*/
    std::ofstream ficheroSalida(fileName);
    /* Se itera fichero de salida con el iterador cambio de linea,
     * para ser copiado en un vector (vectordeSalida)*/
     std::ostream_iterator <float> iteradordeSalida(ficheroSalida,"\n");
     /* Se copian los elementos del iterador en el vector de datos. */
     std::copy(vectorDatos.begin(),vectorDatos.end(),iteradordeSalida);

}
/*Se desarrolla la funcion de conversion de matriz Eigen a Fichero.
 * Funcion util dado que los valores parciales que se obtienen se imprimen en ficheros
 * para tener seguridad y trazabilidad.
 */

void ExtraerData::EigentoFile(Eigen::MatrixXd matrixData,std::string fileName ){
    std::ofstream ficheroSalida(fileName);
    if(ficheroSalida.is_open()){
        ficheroSalida<<matrixData<<"\n";
    }

}





















