#ifndef LINEALREGRESION_H
#define LINEALREGRESION_H

#include <iostream>
#include <Eigen/Dense>
#include <string>
#include <fstream>
class LinealRegresion
{
public:
    LinealRegresion(){}
    float FunCostOLS (Eigen::MatrixXd X,Eigen::MatrixXd y, Eigen::MatrixXd Theta);
    std::tuple <Eigen::VectorXd, std::vector<float>> GradDesc(Eigen::MatrixXd X,Eigen::MatrixXd y,Eigen::VectorXd Theta, float alpha,  int iteraciones);
    float RSquared(Eigen::MatrixXd y, Eigen::MatrixXd y_hat);
};

#endif // LINEALREGRESION_H
